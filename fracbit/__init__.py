from __future__ import annotations

from fractions import Fraction
from math import floor, lcm
from typing import TYPE_CHECKING

if TYPE_CHECKING:
    from collections.abc import Sequence
    from typing import Never


def _fraction_and(self: Fraction, __value: Fraction, /) -> Fraction:
    s_int, s_frac = _split_to_int_and_frac(self)
    v_int, v_frac = _split_to_int_and_frac(__value)
    s_finite, s_repetend = _split_to_finite_and_repetend(s_frac)
    v_finite, v_repetend = _split_to_finite_and_repetend(v_frac)
    s_finite, s_repetend, v_finite, v_repetend = _fix_length(
        s_finite, s_repetend, v_finite, v_repetend
    )
    finite = _seq_bool_and(s_finite, v_finite)
    repetend = _seq_bool_and(s_repetend, v_repetend)
    frac = _unite_from_finite_and_repetend(finite, repetend)
    return (s_int & v_int) + frac


def _fraction_xor(self: Fraction, __value: Fraction, /) -> Fraction:
    return self + __value - 2 * _fraction_and(self, __value)


def _fraction_or(self: Fraction, __value: Fraction, /) -> Fraction:
    return self + __value - _fraction_and(self, __value)


def _split_to_int_and_frac(a: Fraction) -> tuple[int, Fraction]:
    i = floor(a)
    return i, a - i


def _split_to_finite_and_repetend(a: Fraction) -> tuple[
    tuple[bool, ...],
    tuple[bool, ...],
]:
    assert 0 <= a < 1

    if not a.numerator:
        return (), (False,)

    # step 1
    finite: list[bool] = []
    hare = a.numerator
    tortoise = a.numerator
    while True:
        hare *= 2
        hare %= a.denominator
        hare *= 2
        hare %= a.denominator
        tortoise *= 2
        q, tortoise = divmod(tortoise, a.denominator)
        finite.append(bool(q))
        if not tortoise:
            return tuple(finite), (False,)
        if hare == tortoise:
            finite.clear()
            break

    # step 2
    if hare != a.numerator:
        hare = a.numerator
        while True:
            hare *= 2
            q, hare = divmod(hare, a.denominator)
            finite.append(bool(q))
            tortoise *= 2
            tortoise %= a.denominator
            if hare == tortoise:
                break

    # step 3
    repetend: list[bool] = []
    while True:
        hare *= 2
        hare %= a.denominator
        hare *= 2
        hare %= a.denominator
        tortoise *= 2
        q, tortoise = divmod(tortoise, a.denominator)
        repetend.append(bool(q))
        if hare == tortoise:
            break

    return tuple(finite), tuple(repetend)


def _fix_length(
    a_finite: tuple[bool, ...],
    a_repetend: tuple[bool, ...],
    b_finite: tuple[bool, ...],
    b_repetend: tuple[bool, ...],
) -> tuple[
    tuple[bool, ...],
    tuple[bool, ...],
    tuple[bool, ...],
    tuple[bool, ...],
]:
    a_length_diff = max(len(b_finite) - len(a_finite), 0)
    b_length_diff = max(len(a_finite) - len(b_finite), 0)
    a_finite = (
        a_finite
        + a_repetend * (a_length_diff // len(a_repetend))
        + a_repetend[: a_length_diff % len(a_repetend)]
    )
    b_finite = (
        b_finite
        + b_repetend * (b_length_diff // len(b_repetend))
        + b_repetend[: b_length_diff % len(b_repetend)]
    )
    a_repetend = (
        a_repetend[a_length_diff % len(a_repetend) :]
        + a_repetend[: a_length_diff % len(a_repetend)]
    )
    b_repetend = (
        b_repetend[b_length_diff % len(b_repetend) :]
        + b_repetend[: b_length_diff % len(b_repetend)]
    )

    lcm_ab = lcm(len(a_repetend), len(b_repetend))
    a_times = lcm_ab // len(a_repetend)
    b_times = lcm_ab // len(b_repetend)
    a_repetend = a_repetend * a_times
    b_repetend = b_repetend * b_times

    return a_finite, a_repetend, b_finite, b_repetend


def _seq_bool_and(a: Sequence[bool], b: Sequence[bool]) -> tuple[bool, ...]:
    assert len(a) == len(b)
    return tuple(a_bit and b_bit for a_bit, b_bit in zip(a, b))


def _unite_from_finite_and_repetend(
    finite: tuple[bool, ...], repetend: tuple[bool, ...]
) -> Fraction:
    assert any(not bit for bit in repetend)

    numerator = _seq_bool_to_int(finite + repetend) - _seq_bool_to_int(finite)
    denominator = 2 ** (len(finite) + len(repetend)) - 2 ** len(finite)
    return Fraction(numerator, denominator)


def _seq_bool_to_int(a: Sequence[bool]) -> int:
    result = 0
    for bit in a:
        result *= 2
        result += 1 if bit else 0
    return result


def todo() -> Never:
    raise NotImplementedError


setattr(Fraction, "__and__", _fraction_and)
setattr(Fraction, "__xor__", _fraction_xor)
setattr(Fraction, "__or__", _fraction_or)
