# pyright: reportPrivateUsage=false
from __future__ import annotations

from fractions import Fraction

import pytest

from fracbit import (
    _fix_length,
    _seq_bool_and,
    _seq_bool_to_int,
    _split_to_finite_and_repetend,
    _split_to_int_and_frac,
    _unite_from_finite_and_repetend,
)


@pytest.mark.parametrize(
    ("a", "i", "f"),
    [
        (Fraction(1, 3), 0, Fraction(1, 3)),
        (Fraction(39, 4), 9, Fraction(3, 4)),
        (Fraction(15, 3), 5, Fraction(0, 1)),
        (Fraction(-12, 4), -3, Fraction(0, 1)),
        (Fraction(-3, 7), -1, Fraction(4, 7)),
    ],
)
def test_split_to_int_and_frac(a: Fraction, i: int, f: Fraction) -> None:
    i_, f_ = _split_to_int_and_frac(a)
    assert i_ == i
    assert f_ == f


@pytest.mark.parametrize(
    ("a", "f", "r"),
    [
        (Fraction(0, 1), (), (False,)),
        (Fraction(1, 2), (True,), (False,)),
        (Fraction(1, 4), (False, True), (False,)),
        (Fraction(1, 3), (), (False, True)),
        (Fraction(2, 3), (), (True, False)),
    ],
)
def test_split_to_finite_and_repetend(
    a: Fraction, f: tuple[bool, ...], r: tuple[bool, ...]
) -> None:
    f_, r_ = _split_to_finite_and_repetend(a)
    assert f_ == f
    assert r_ == r


@pytest.mark.parametrize(
    ("af", "ar", "bf", "br", "af2", "ar2", "bf2", "br2"),
    [
        ((), (False,), (), (False,), (), (False,), (), (False,)),
        (
            (False, True, True),
            (True, False, False, True),
            (True, False, True),
            (False, True, False, True),
            (False, True, True),
            (True, False, False, True),
            (True, False, True),
            (False, True, False, True),
        ),
        (
            (False, True),
            (True, False, False),
            (False, True),
            (True, False),
            (False, True),
            (True, False, False, True, False, False),
            (False, True),
            (True, False, True, False, True, False),
        ),
        (
            (True, True, True),
            (True, False, False),
            (False, False),
            (True, False, True),
            (True, True, True),
            (True, False, False),
            (False, False, True),
            (False, True, True),
        ),
        (
            (True, False, True, True),
            (True, False, False),
            (False, True, True),
            (True, False),
            (True, False, True, True),
            (True, False, False, True, False, False),
            (False, True, True, True),
            (False, True, False, True, False, True),
        ),
        (
            (True, False),
            (False, True, True),
            (False, False, True, True, False, True),
            (True, False, True, False),
            (True, False, False, True, True, False),
            (
                True,
                True,
                False,
                True,
                True,
                False,
                True,
                True,
                False,
                True,
                True,
                False,
            ),
            (False, False, True, True, False, True),
            (
                True,
                False,
                True,
                False,
                True,
                False,
                True,
                False,
                True,
                False,
                True,
                False,
            ),
        ),
        (
            (True, False, False, True),
            (True, False),
            (False, False, True),
            (False,),
            (True, False, False, True),
            (True, False),
            (False, False, True, False),
            (False, False),
        ),
        (
            (False, False, True),
            (True, False, False),
            (),
            (True, False),
            (False, False, True),
            (True, False, False, True, False, False),
            (True, False, True),
            (False, True, False, True, False, True),
        ),
    ],
)
def test_fix_length(
    af: tuple[bool, ...],
    ar: tuple[bool, ...],
    bf: tuple[bool, ...],
    br: tuple[bool, ...],
    af2: tuple[bool, ...],
    ar2: tuple[bool, ...],
    bf2: tuple[bool, ...],
    br2: tuple[bool, ...],
) -> None:
    af2_, ar2_, bf2_, br2_ = _fix_length(af, ar, bf, br)
    assert af2_ == af2
    assert ar2_ == ar2
    assert bf2_ == bf2
    assert br2_ == br2


@pytest.mark.parametrize(
    ("a", "b", "c"),
    [
        ((), (), ()),
        (
            (False, False, True, True),
            (False, True, False, True),
            (False, False, False, True),
        ),
    ],
)
def test_seq_bool_and(
    a: tuple[bool, ...], b: tuple[bool, ...], c: tuple[bool, ...]
) -> None:
    c_ = _seq_bool_and(a, b)
    assert c_ == c


@pytest.mark.parametrize(
    ("a", "f", "r"),
    [
        (Fraction(0, 1), (), (False,)),
        (Fraction(1, 2), (True,), (False,)),
        (Fraction(1, 4), (False, True), (False,)),
        (Fraction(1, 3), (), (False, True)),
        (Fraction(2, 3), (), (True, False)),
    ],
)
def test_unite_from_finite_and_repetend(
    a: Fraction, f: tuple[bool, ...], r: tuple[bool, ...]
) -> None:
    a_ = _unite_from_finite_and_repetend(f, r)
    assert a_ == a


@pytest.mark.parametrize(
    ("a", "n"),
    [
        ((), 0),
        ((True,), 1),
        ((True, False), 2),
        ((True, False, True), 5),
        ((True, False, True, False, True, False), 42),
    ],
)
def test_seq_bool_to_int(a: tuple[bool, ...], n: int) -> None:
    n_ = _seq_bool_to_int(a)
    assert n_ == n


@pytest.mark.parametrize(
    ("a"),
    [
        (Fraction(40, 78)),
        (Fraction(6, 115)),
        (Fraction(91, 181)),
        (Fraction(7, 29)),
        (Fraction(26, 108)),
        (Fraction(72, 197)),
        (Fraction(169, 229)),
        (Fraction(10, 18)),
        (Fraction(238, 241)),
        (Fraction(46, 231)),
        (Fraction(200, 235)),
        (Fraction(7, 22)),
        (Fraction(8, 119)),
        (Fraction(49, 230)),
        (Fraction(44, 225)),
        (Fraction(10, 54)),
        (Fraction(22, 237)),
        (Fraction(70, 81)),
        (Fraction(9, 236)),
        (Fraction(14, 94)),
        (Fraction(237, 247)),
        (Fraction(92, 160)),
        (Fraction(0, 13)),
        (Fraction(64, 81)),
        (Fraction(79, 255)),
        (Fraction(0, 9)),
        (Fraction(243, 245)),
        (Fraction(93, 123)),
        (Fraction(109, 252)),
        (Fraction(22, 152)),
        (Fraction(9, 184)),
        (Fraction(15, 55)),
        (Fraction(55, 117)),
        (Fraction(10, 65)),
        (Fraction(130, 132)),
        (Fraction(161, 214)),
        (Fraction(47, 48)),
        (Fraction(62, 66)),
        (Fraction(12, 47)),
        (Fraction(59, 218)),
        (Fraction(104, 176)),
        (Fraction(14, 35)),
        (Fraction(51, 52)),
        (Fraction(5, 60)),
        (Fraction(36, 185)),
        (Fraction(14, 99)),
        (Fraction(56, 121)),
        (Fraction(28, 50)),
        (Fraction(171, 184)),
        (Fraction(172, 182)),
    ],
)
def test_split_and_unite_finite_and_repetend(a: Fraction) -> None:
    f, r = _split_to_finite_and_repetend(a)
    a_ = _unite_from_finite_and_repetend(f, r)
    assert a_ == a
