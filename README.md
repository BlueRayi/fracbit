# fracbit

Bitwise Operator for Python `Fraction`.

## Usage

```python
import fracbit
```

There is nothing further to explain.

## Detailed Explanation

See Qiita article.

https://qiita.com/BlueRayi/items/564b8822dce3f577c3f4
